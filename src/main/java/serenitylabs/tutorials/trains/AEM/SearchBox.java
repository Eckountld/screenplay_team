package serenitylabs.tutorials.trains.AEM;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class SearchBox {



    public static Target SEARCH_FIELD_NAME = Target.the("search field").located(By.name("q"));


    public static Target SEARCH_FIELD_ID = Target.the("search field").located(By.id(""));


    public static Target SEARCH_FIELD_CLASS = Target.the("search field").located(By.className(""));


    public static Target SEARCH_FIELD_TAG = Target.the("search field").located(By.tagName(""));

}
