package serenitylabs.tutorials.trains.BDD;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.wikipedia.org/")
public class WikiSearchPage extends PageObject {}
