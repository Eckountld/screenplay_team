package serenitylabs.tutorials.trains.BDD;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.openqa.selenium.Keys.ENTER;

public class BDD_Search implements Task {
    private final String searchTerm;

    protected BDD_Search(String searchTerm) {
        this.searchTerm = searchTerm;
    }


    @Step("Search for #searchTerm")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(searchTerm)
                        .into(BDD_SearchBox.SEARCH_FIELD_NAME)
                        .thenHit(ENTER)
        );
    }

    public static BDD_Search forTheTerm(String searchTerm) {
        return instrumented(BDD_Search.class, searchTerm);
    }
}
