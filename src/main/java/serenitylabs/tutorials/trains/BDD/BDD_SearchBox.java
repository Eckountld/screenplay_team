package serenitylabs.tutorials.trains.BDD;


import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class BDD_SearchBox {

    public static Target SEARCH_FIELD_NAME = Target.the("search field").located(By.name("search"));


    public static Target SEARCH_FIELD_ID = Target.the("search field").located(By.id(""));


    public static Target SEARCH_FIELD_CLASS = Target.the("search field").located(By.className(""));


    public static Target SEARCH_FIELD_TAG = Target.the("search field").located(By.tagName(""));

}
