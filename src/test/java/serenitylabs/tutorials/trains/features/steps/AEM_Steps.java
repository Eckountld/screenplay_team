package serenitylabs.tutorials.trains.features.steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.questions.page.TheWebPage;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import serenitylabs.tutorials.trains.AEM.AEM;
import serenitylabs.tutorials.trains.OpenTheApplication;
import serenitylabs.tutorials.trains.AEM.Search;

import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.containsString;


public class AEM_Steps {

               @Managed(uniqueSession = true)
               WebDriver herBrowser;
     AEM aem = new AEM();
    Actor anna = Actor.named("Lean");

    OpenTheApplication openTheApplication = new OpenTheApplication(aem);
    @Before
    public void set_the_stage()throws  Throwable{
        OnStage.setTheStage(new OnlineCast());}

    @When("^I will go into google$")
    public void i_will_go_into_google() throws Exception {
        anna.can(BrowseTheWeb.with(herBrowser));
        givenThat(anna).wasAbleTo(openTheApplication);
    }

    @When("^Search for the keyword AEM$")
    public void search_for_the_keyword_AEM() throws Exception {
        when(anna).attemptsTo(Search.forTheTerm("AEM"));
}

    @Then("^I see if the google page contains the website for AEM$")
    public void i_see_if_the_google_page_contains_the_website_for_AEM() throws Exception {
        then(anna).should(eventually(seeThat(TheWebPage.title(), containsString("AEM"))));
    }



}
