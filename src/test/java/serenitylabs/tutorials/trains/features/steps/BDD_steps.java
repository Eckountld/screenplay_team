package serenitylabs.tutorials.trains.features.steps;


import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.EventualConsequence;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.questions.page.TheWebPage;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;
import serenitylabs.tutorials.trains.BDD.BDD_Search;
import serenitylabs.tutorials.trains.BDD.WikiSearchPage;
import serenitylabs.tutorials.trains.OpenTheApplication;

import static org.hamcrest.core.StringContains.containsString;

public class BDD_steps {

    Actor lean = Actor.named("Lean");
    WikiSearchPage wikipage = new WikiSearchPage();

    @Managed(uniqueSession = true)
    WebDriver browser;

    @Steps
    OpenTheApplication opentheapplication = new OpenTheApplication(wikipage);

    @Before
    public void set_the_stage()throws  Throwable{
        OnStage.setTheStage(new OnlineCast());}


    @When("^I will go into wiki$")
    public void i_will_go_into_wiki() throws Exception {

        lean.can(BrowseTheWeb.with(browser));
        GivenWhenThen.givenThat(lean).wasAbleTo(opentheapplication);
    }

    @When("^Search for the word Behaviour Driven Development$")
    public void search_for_the_word_Behaviour_Driven_Development() throws Exception {
      GivenWhenThen.when(lean).attemptsTo(BDD_Search.forTheTerm("Behaviour Driven Development"));
    }

    @Then("^I see if the current page contains the title of Behaviour Driven Development$")
    public void i_see_if_the_current_page_contains_the_title_of_Behaviour_Driven_Development() throws Exception {
        GivenWhenThen.then(lean).should(EventualConsequence.eventually(GivenWhenThen.seeThat(TheWebPage.title(), containsString("Behavior-driven development - Wikipedia"))));
    }

}
